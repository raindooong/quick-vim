# quickVim
vim配置，安装简单，拿来即用，跨平台兼容linux/mac和windows。

```
               _      __  _    ___
  ____ ___  __(_)____/ /_| |  / (_)___ ___
 / __ `/ / / / / ___/ //_/ | / / / __ `__ \
/ /_/ / /_/ / / /__/ ,<  | |/ / / / / / / /
\__, /\__,_/_/\___/_/|_| |___/_/_/ /_/ /_/
  /_/
```

![image](https://gitee.com/liuvvming/quick-vim/raw/master/images/vim_startup.png)

# 特性
* 基于cscope/ctags的搜索与跳转功能;
* 基于自定义规则的Omni全能代码补全;
* 方便快捷的文件切换, 脱离鼠标的束缚;
* 近千种主题可供选择, 真正的主题自由;

# 安装

## linux/mac 安装方法
```bash
1. cd quick-vim

2. source ./install.sh
```
## windows 安装方法

```bash
1. 预先安装[Git](http://git-scm.com/)(必选, 在git bash终端中使用vim)

2. 安装git后，打开git bash终端

3. cd quick-vim

4. source ./install.sh

5. 添加项目下的tool_for_windows到环境变量中, 重启或注销后即可在windows下使用cscope和ctags

```
![image](https://gitee.com/liuvvming/quick-vim/raw/master/images/vim_startup.gif)


# 依赖工具
``` 工具
* ctags/cscope               // 代码浏览跳转工具，linux下安装方法：apt-get install ctags cscope

* astyle/clang-format        // 代码格式化工具，linux下安装方法：apt-get install astyle clang-format
```

# cscope使用说明
``` cscope
# cscope使用方法, 详见cscope.sh

1. cd <proj_dir>; 进入工程目录

2. source ~/cscope.sh; 在工程目录生成cscope.out,ctags等文件

3. 在终端(windows下使用git bash)中打开vim, 即可实现代码跳转
```

# astyle使用说明
```
# linux/mac下的使用方法：
> source /path/quick_vim/astyle.sh          // 格式化当前文件夹下代码
> source /path/quick_vim/astyle.sh -r       // 递归格式化当前文件夹下所有代码
> source /path/quick_vim/astyle.sh -f xx.c  // 格式化指定文件代码

# windows下的使用方法见tool_for_windows中的readme.txt
```

# vim中使用的插件
``` 插件
* a.vim                      // .c/.h文件切换
* autocomplpop               // 配合Omni 实现代码补全
* cscope.vim                 // 类似ctags
* DoxygenToolkit.vim         // Doxygen风格的注释
* nerdcommenter              // 快速注释/解开注释
* nerdtree                   // 显示树形目录
* rainbow                    // 彩色显示括号对
* tabular                    // 对齐文本
* taglist.vim                // 显示文件的标签, 比如文件的类、结构体、函数、变量等
* undotree                   // 记住对文本状态的最新更改
* vim-autoformat             // 代码自动化格式
* vim-choosewin              // 窗口管理器
* vim-colorschemes           // 主题美化
* vim-airline-themes         // 主题美化
* vim-airline                // 主题美化
* vim-fugitive               // git相关的操作
* vim-cpp-enhanced-highlight // 语法关键字高亮
* vim-interestingwords       // 单词高亮
* vim-startify               // 浏览最近打开的文件
* vim-trailing-whitespace    // 高亮行末空格(标红)
```

# vim的快捷键
``` 快捷键
正常模式下的快捷键（非插入模式）配置, 可以根据个人习惯在vimrc中修改

F4       // 快速切换.h和c/cpp文件
F2       // 打开当前函数和变量目录树显示在屏幕右侧
F3       // 打开文件目录树显示在屏幕左侧

<C-j>    // 跳转到变量或者函数定义的地方
<C-h>    // 按顺序光标跳转各个窗口

,w       // 保存文件
,q       // 退出当前窗口
,qa      // 退出全部窗口
,1       // 仅保留当前窗口
,2       // 仅保留当前窗口，并且打开文件目录树显示在屏幕左侧

,s       // 水平分屏
,v       // 竖直分屏

,=       // 文本对齐
,<space> // 去除行尾空格
,m       // 去除行尾特殊回车换行^M

<C-p>    // 查找当前目录中文件
<S-p>    // 查找缓存中文件

,fs      // 基于cscope, 查找项目内关键字，
,ff      // 基于cscope, 查找项目内文件名
,o       // 打开cscope搜索结果
<C-a>    // 向前查找
<C-e>    // 向后查找

,fmt     // astyle 代码格式化
,cf      // clang-format 代码格式化

```

