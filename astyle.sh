#!/bin/sh

# ## 使用方法：
# source /path/quick_vim/astyle.sh     格式化当前文件夹下代码
# source /path/quick_vim/astyle.sh -r  递归查找子文件夹
# source /path/quick_vim/astyle.sh -f xx.c  格式化指定文件代码

SRC_PATH=`pwd`

recursive=0
file=0

echo $SHELL

if [ $SHELL != "/bin/zsh" ]; then
    HEAD=""
    LOG_COLOR_BLACK=""
    LOG_COLOR_RED=""
    LOG_COLOR_GREEN=""
    LOG_COLOR_BROWN=""
    LOG_COLOR_BLUE=""
    LOG_COLOR_PURPLE=""
    LOG_COLOR_CYAN=""
    LOG_RESET_COLOR=""
else
    HEAD="\033[0;"
    LOG_COLOR_BLACK=${HEAD}30"m"
    LOG_COLOR_RED=${HEAD}31"m"
    LOG_COLOR_GREEN=${HEAD}32"m"
    LOG_COLOR_BROWN=${HEAD}33"m"
    LOG_COLOR_BLUE=${HEAD}34"m"
    LOG_COLOR_PURPLE=${HEAD}35"m"
    LOG_COLOR_CYAN=${HEAD}36"m"
    # LOG_COLOR(COLOR)  "\033[0;" COLOR "m"
    # LOG_BOLD(COLOR)   "\033[1;" COLOR "m"
    LOG_RESET_COLOR="\033[0m"
fi

# set source path
case $1 in
    -r)
        recursive=1
        echo ${LOG_COLOR_BLUE}" recursive path: ${SRC_PATH}"
        ;;
    -f)
        file=1
        echo ${LOG_COLOR_BLUE}" astyle file: ${SRC_PATH}/$2"
        ;;
esac

cd ${SRC_PATH}
echo ${LOG_COLOR_CYAN}"# switch to $SRC_PATH"
echo ${LOG_COLOR_CYAN}"---------"

#--indent=spaces=4            \
#--style=kr                   \
#--indent-labels              \
#--pad-oper                   \
#--unpad-paren                \
#--pad-header                 \
#--keep-one-line-statements   \
#--convert-tabs               \
#--indent-preprocessor        \
#--align-pointer=name         \
#--align-reference=name       \
#--keep-one-line-blocks       \
#--keep-one-line-statements   \
#--attach-namespaces          \
#--max-instatement-indent=120 \

if [ 1 -eq ${recursive} ]; then

    astyle *.c *.h *.cpp --recursive \
        --style=kr                   \
        --lineend=linux              \
        --pad-oper                   \
        --unpad-paren                \
        --pad-header                 \
        --convert-tabs               \
        --indent=spaces=4            \
        --indent-labels              \
        --indent-preprocessor        \
        --align-pointer=name         \
        --align-reference=name       \
        --keep-one-line-blocks       \
        --keep-one-line-statements   \
        --attach-namespaces          \
        --preserve-date              \
        --max-instatement-indent=120 \
        --suffix=none

elif [ 1 -eq ${file} ]; then

    astyle $2                        \
        --style=kr                   \
        --lineend=linux              \
        --pad-oper                   \
        --unpad-paren                \
        --pad-header                 \
        --convert-tabs               \
        --indent=spaces=4            \
        --indent-labels              \
        --indent-preprocessor        \
        --align-pointer=name         \
        --align-reference=name       \
        --keep-one-line-blocks       \
        --keep-one-line-statements   \
        --attach-namespaces          \
        --preserve-date              \
        --max-instatement-indent=120 \
        --suffix=none

else

    astyle *.c *.h                   \
        --style=kr                   \
        --lineend=linux              \
        --pad-oper                   \
        --unpad-paren                \
        --pad-header                 \
        --convert-tabs               \
        --indent=spaces=4            \
        --indent-labels              \
        --indent-preprocessor        \
        --align-pointer=name         \
        --align-reference=name       \
        --keep-one-line-blocks       \
        --keep-one-line-statements   \
        --attach-namespaces          \
        --preserve-date              \
        --max-instatement-indent=120 \
        --suffix=none
fi

echo ${LOG_COLOR_CYAN}"---------"
echo ${LOG_COLOR_CYAN}"# astyle done!"
echo ${LOG_COLOR_GREEN}"---> HAPPY HACKING! <---" ${LOG_RESET_COLOR}

