#!/bin/bash
echo "clean .vimrc.*, .vim.* and cscope.sh.*"

rm -rf ~/.vimrc.*
rm -rf ~/.vim.*
rm -rf ~/cscope.sh.*

