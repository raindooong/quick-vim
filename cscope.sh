#!/bin/sh

# ## 使用方法：
# source ~/cscope.sh [-d|-c|-u] dir
#
# ```
# > cscope.sh脚本同时支持多种命令
# > -d 命令后跟具体的项目代码目录，可以在该目录生成对应的cscope数据库
# > -c 切换数据库，方便有多个项目间切换
# > -u 或者不带参数, 更新cscope数据库
#
# ```
# ## 简化方法：
#
# 1. 在~/.bashrc中添加环境变量：export cs="source ~/cscope.sh"
# 2. 执行$cs等于执行“source ~/cscope.sh”命令, 如：$cs -d ~/work/project_xx

SRC_PATH=`pwd`

update=1 #default for update
change=0

echo $SHELL

if [ $SHELL != "/bin/zsh" ]; then
    HEAD=""
    LOG_COLOR_BLACK=""
    LOG_COLOR_RED=""
    LOG_COLOR_GREEN=""
    LOG_COLOR_BROWN=""
    LOG_COLOR_BLUE=""
    LOG_COLOR_PURPLE=""
    LOG_COLOR_CYAN=""
    LOG_RESET_COLOR=""
else
    HEAD="\033[0;"
    LOG_COLOR_BLACK=${HEAD}30"m"
    LOG_COLOR_RED=${HEAD}31"m"
    LOG_COLOR_GREEN=${HEAD}32"m"
    LOG_COLOR_BROWN=${HEAD}33"m"
    LOG_COLOR_BLUE=${HEAD}34"m"
    LOG_COLOR_PURPLE=${HEAD}35"m"
    LOG_COLOR_CYAN=${HEAD}36"m"
    # LOG_COLOR(COLOR)  "\033[0;" COLOR "m"
    # LOG_BOLD(COLOR)   "\033[1;" COLOR "m"
    LOG_RESET_COLOR="\033[0m"
fi


# set source path
case $1 in
    -d)
        SRC_PATH=$2
        echo ${LOG_COLOR_BLUE}"set source path: ${SRC_PATH}"
        ;;
    -c)
        SRC_PATH=$2
        change=1
        echo ${LOG_COLOR_BLUE}"change source path: ${SRC_PATH}"
        ;;
    -u|*)
        update=1
        echo ${LOG_COLOR_BLUE}"update source path: ${SRC_PATH}"
esac

cd ${SRC_PATH}
echo ${LOG_COLOR_CYAN}"# switch to $SRC_PATH"

if [ 1 -eq ${change} ]; then
    echo ${LOG_COLOR_BROWN}"# change project cscope database!"
    res=$(find ${SRC_PATH} -name cscope.out)

    if [ "x"${res} = "x" ]; then
        # echo "# not found cscope database, create cscope.files..."
        # find ${SRC_PATH} -name "*.h" -o -name "*.c" -o -name "*.cc" > cscope.files

        echo ${LOG_COLOR_BROWN}"# create cscope.out..."
        # cscope -bkq -i cscope.files
        cscope -bRq

        echo ${LOG_COLOR_BROWN}"# create tags..."
        # ctags -R *
        ctags -R --C-kinds=+p --fields=+aS --extra=+q
    else
        echo ${LOG_COLOR_BROWN}"found cscope database:${res}, just change work path"
    fi
elif [ 1 -eq ${update} ]; then
    # echo "# update cscope.files..."
    # find ${SRC_PATH} -name "*.h" -o -name "*.c" -o -name "*.cc" > cscope.files

    echo ${LOG_COLOR_BROWN}"# update cscope database..."
    # cscope -bkq -i cscope.files
    cscope -bRq

    echo ${LOG_COLOR_BROWN}"# create tags..."
    ctags -R --C-kinds=+p --fields=+aS --extra=+q

    export CSCOPE_DB=${SRC_PATH}/cscope.out
fi

echo ${LOG_COLOR_CYAN}"# done!"
echo ${LOG_COLOR_GREEN}"---> HAPPY HACKING! <---" ${LOG_RESET_COLOR}

