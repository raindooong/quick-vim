#!/bin/bash

function digitaldatetime() {
    echo `date +"%Y%m%d%H%M%S"`
}

quickVim=`pwd -P`

cd $HOME

echo "Start to install vim config"
echo "Looking for an existing vim config..."

if [ -f ~/.vimrc ] || [ -h ~/.vimrc ]; then
    echo "Found ~/.vimrc. Backing up to ~/.vimrc.`digitaldatetime`";
    mv ~/.vimrc ~/.vimrc.`digitaldatetime`;
fi

if [ -f ~/.clang-format ] || [ -h ~/.clang-format ]; then
    echo "Found ~/.clang-format. Backing up to ~/.clang-format.`digitaldatetime`";
    mv ~/.clang-format ~/.clang-format.`digitaldatetime`;
fi

if [ -f ~/cscope.sh ] || [ -h ~/cscope.sh ]; then
    echo "Found ~/cscope.sh. Backing up to ~/cscope.sh.`digitaldatetime`";
    mv ~/cscope.sh ~/cscope.sh.`digitaldatetime`;
fi

if [ -d ~/.vim ]; then
    echo "Found ~/.vim. Backing up to ~/.vim.`digitaldatetime`";
    mv ~/.vim ~/.vim.`digitaldatetime`;
fi

echo "Copying .vimrc and .vim"

echo "ln -s ${quickVim}/vim .vim"
ln -s ${quickVim}/vim .vim

echo "ln -s ${quickVim}/vimrc .vimrc"
ln -s ${quickVim}/vimrc .vimrc

echo "ln -s ${quickVim}/clang-format .clang-format"
ln -s ${quickVim}/clang-format .clang-format

echo "ln -s ${quickVim}/cscope.sh cscope.sh"
ln -s ${quickVim}/cscope.sh cscope.sh
echo " "

#                _      __  _    ___
#   ____ ___  __(_)____/ /_| |  / (_)___ ___
#  / __ `/ / / / / ___/ //_/ | / / / __ `__ \
# / /_/ / /_/ / / /__/ ,<  | |/ / / / / / / /
# \__, /\__,_/_/\___/_/|_| |___/_/_/ /_/ /_/
#   /_/

echo "\033[0;34m"'                 _      __  _    ___          '"\033[0m"
echo "\033[0;34m"'    ____ ___  __(_)____/ /_| |  / (_)___ ___  '"\033[0m"
echo "\033[0;34m"'   / __ `/ / / / / ___/ //_/ | / / / __ `__ \ '"\033[0m"
echo "\033[0;34m"'  / /_/ / /_/ / / /__/ ,<  | |/ / / / / / / / '"\033[0m"
echo "\033[0;34m"'  \__, /\__,_/_/\___/_/|_| |___/_/_/ /_/ /_/  '"\033[0m"
echo "\033[0;34m"'    /_/                                       '"\033[0m"

echo " "
echo "\033[0;32m""  Happy coding!"
